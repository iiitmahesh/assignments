var express = require("express");
require("dotenv").config();
var port = process.env.PORT ? process.env.PORT:3000;
var bodyParser = require("body-parser");
var cors = require("cors");
//connect to MongoDB
global.db = require("./config/db.js");

var app = express();
app.use(bodyParser.json({ limit: "100mb" }));
// app.set("view engine", "pug");
app.use(cors());

const appController = require("./api/controllers/appController.js");

	app.route('/').get((req, res) => {
		res.send({
		  status: 'OK',
		  message: 'server running'
		});
	});


	app.route('/api/ping').get((req, res) => {
		res.send({
		  status: 'OK',
		  message: 'pong'
		});
	});



  app.route("/api/ticket").post(appController.create_ticket);
  app.route("/api/ticket/:id").put(appController.update_ticket);
  app.route("/api/ticket/:id").delete(appController.delete_ticket);
  app.route("/api/ticket/:id").get(appController.get_ticket);
  app.route("/api/tickets/getAll").get(appController.getAll_ticket);

  app.route("/api/comment").post(appController.create_comment);
  app.route("/api/comment/:id").put(appController.update_comment);
  app.route("/api/comment/:id").delete(appController.delete_comment);
  app.route("/api/comment/:id").get(appController.get_comment);
  app.route("/api/comments/getAll").get(appController.getAll_comment);


app.listen(port);
console.log("listening on " + port);
