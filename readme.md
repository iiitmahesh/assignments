
git clone 'repourl'

npm install

update .env

node index.js

.env

PORT=8080
DB_ENV="dev"

MONGO_URI="localhost:27017"
MONGO_PORT=27017
MONGO_DB_NAME="test"
MONGO_USERNAME="localhost"
MONGO_PASSWORD="localhost"



API

  serverUrl ='http://localhost:8080'

=> GET => '/api/ping'
=> DELETE =>"/api/ticket/:id"
=> GET =>"/api/ticket/:id"
=> GET =>"/api/ticket/getAll"
=> POST => "/api/ticket" 
=> PUT =>"/api/ticket/:id"

=> DELETE =>"/api/comment/:id"
=> GET =>"/api/comment/:id"
=> GET =>"/api/comments/getAll"
=> POST => "/api/comment" 
=> PUT =>"/api/comment/:id"


sample Object of comment:

{
	"name":"hello world",
	"type":"comment",
	"rating":"4",
	"favorite":"indian"
}

