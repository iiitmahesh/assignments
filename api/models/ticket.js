var mongoose = require("mongoose");

var TicketSchema = new mongoose.Schema(
  {
    title: {
      type: String
    },
    description: {
      type: String
    },
    docs: {
      type: String
    },
    dueDate: {
      type: String
    },
    assignee: {
      type: String
    },
    status: {
      type: String,
      enum: ["ACTIVE","IN-PROGRESS","CLOSED", "DELETED"],
      default: "ACTIVE"
    },
  },
  {
    timestamps: true
  }
);

var Ticket = mongoose.model("Ticket", TicketSchema);

module.exports = Ticket;
