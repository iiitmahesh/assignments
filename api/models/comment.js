var mongoose = require("mongoose");

var CommentSchema = new mongoose.Schema(
  {
    ticket:{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Ticket",
    },
    message: {
      type: String
    },
    type: {
      type: String
    },
    notifyTo: {
      type: String
    },
    assignTo: {
      type: String
    },
    status: {
      type: String,
      enum: ["ACTIVE", "DELETED"],
      default: "ACTIVE"
    },
  },
  {
    timestamps: true
  }
);

var Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;
