var Comment = require("../models/comment.js");
var Ticket = require("../models/ticket.js");

var async = require("async");

module.exports = {
  create_comment: async (req, res) => {
    let data = req.body;
    console.log(data)
    Comment.create(data, (error, doc) => {
      if (error) {
        res.send({ message: "comment not created", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  update_comment: async (req, res) => {
    let data = req.body;
    var id = req.params.id;
    // console.log(data,id)
    Comment.findOneAndUpdate({ _id: id}, { $set: req.body }, (error, doc) => {
        console.log(error,doc)

      if (error) {
        res.send({ message: "comment not updated", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  delete_comment: async (req, res) => {
    var id = req.params.id;
    Comment.remove({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "comment not deleted", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  get_comment: async (req, res) => {
    let id = req.params.id;
    Comment.findOne({ _id: id })
    .populate('ticket')
    .exec(function(error, doc) {
    // Comment.findOne({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "comment not found", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  getAll_comment: async (req, res) => {
    Comment.find({}, (error, docs) => {
      if (error) {
        res.send({ message: "comments not found", error: error });
      } else {
        res.send({ payload: docs });
      }
    });
  },

  // start tickets
  create_ticket: async (req, res) => {
    let data = req.body;
    console.log(data)
    Ticket.create(data, (error, doc) => {
      if (error) {
        res.send({ message: "ticket not created", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  update_ticket: async (req, res) => {
    let data = req.body;
    var id = req.params.id;
    // console.log(data,id)
    Ticket.findOneAndUpdate({ _id: id}, { $set: req.body }, (error, doc) => {
        console.log(error,doc)

      if (error) {
        res.send({ message: "ticket not updated", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  delete_ticket: async (req, res) => {
    var id = req.params.id;
    Ticket.remove({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "ticket not deleted", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  get_ticket: async (req, res) => {
    let id = req.params.id;
    Ticket.findOne({ _id: id }, (error, doc) => {
      if (error) {
        res.send({ message: "ticket not found", error: error });
      } else {
        res.send({ payload: doc });
      }
    });
  },
  getAll_ticket: async (req, res) => {
    Ticket.find({}, (error, docs) => {
      if (error) {
        res.send({ message: "ticket not found", error: error });
      } else {
        res.send({ payload: docs });
      }
    });
  }
};
